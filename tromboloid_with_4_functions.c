//WAP to find the volume of a tromboloid using 4 functions.

#include<stdio.h>
#include<math.h>

float input()
{
    float a;
    scanf("%f,",&a);
    return a;
}

float find_volume(float h,float b,float d)
{
    float vol;
    vol=((1.0/3.0)*((h*d)+d))/b;
    return vol;
}

void output(float h,float b,float d)
{
    printf("volume=%f\n",h,b,d);
}

int main()
{
    float x,y,z,v;
    printf("Enter the height\n");
    x=input();
    printf("Enter the base\n");
    y=input();
    printf("Enter the characteristic dimension\n");
    z=input();
    v=find_volume(x,y,z);
    output(x,y,z);
    return 0;
}
