//Write a program to find the volume of a tromboloid using one function

#include<stdio.h>
#include<math.h>

int main()
{
    float h,b,d,vol;
    printf("Enter height, base and characteristic dimension\n");
    scanf("%f%f%f",&h,&b,&d);
    vol=((1.0/3.0)*((h*d)+d))/b;
    printf("volume=%f",vol);
  
}
    