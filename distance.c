//WAP to find the distance between two point using 4 functions.

float input()
{
    float x1,x2,y1,y2;
    printf("Enter abscissa of first and second point\n");
    scanf("%f",&x1,&x2);
    printf("Enter ordinate of first and second point\n");
    scanf("%f",&y1,&y2);
}

float find_distance(float x1,float x2,float y1,float y2)
{
    float d;
    d=sqrt((pow((x2-x1),2.0))+(pow((y2-y1),2.0)));
    return d;
}

void output(float result)
{
   printf("distance=%f",result);
}
int main()
{
    float x1,x2,y1,y2,result;
    x1=input();
    x2=input();
    y1=input();
    y2=input();
    result=find_distance(x1,x2,y1,y2);
    output(result);
    return 0;
}